# emulate-telematics-broadcast

This task tests the ability to work with RESTful APIs, JSON objects, SQL databases and orchestrating the resources using Python Flask or Django Framework

## Background on this task

We work with telematics data intensively. 
Capturing them in a scalable way from different types of telematics providers is an important task across for us. 

In order to achieve this, the following skills will be demanded: 
1. Write multi-threaded apps
2. Task scheduling
3. Anomoly detection

## This particular challenge

The following is the scope of this task: 

- [ ] Create a server where you have two endpoints broadcasting a payload which is listed under the example payload folders. One endpoint shall broadcast the payload in format-1 and the other in format-2. 
- [ ] Make sure to increment the timestamps and randomize the parameters (latitude, longitude, speed, fuellevel) in the broadcasted payload.
- [ ] Create an App which runs independently and GETs the broadcasted data from the two endpoints implemented above. 
- [ ] Save the captured data in a structured format in the PostgreSQL table with following information ONLY: vehicle-name, timestamp, latitude, longitude, speed, fuellevel. 
- [ ] Please make a note that one table gets created per vehicle and the data is stored under that.

## Skills which will be looked into

- [ ] Ability to write endpoints
- [ ] Error handling in endpoints
- [ ] Ability to write clean code
- [ ] BONUS for a HomeRun: Ability to orchestrate the written Apps using Docker-Compose
